Installation Instructions
=========================

Installing Fabric
-----------------

1. Go to the [Releases](https://gitlab.com/troiganto/sopp-mods/tags) page and
   download the latest release as a ZIP file.
2. Unzip this file and locate `fabric-installer-0.10.2.exe`. (**Note:** On
   Windows computers, the `.exe` suffix might be hidden.)
3. Open this file. In the installer, pick Minecraft version **1.18.1** and
   click **Install**.

Installing Mods
---------------

1. Open a file-browser window. On Windows, you can do this e.g. by clicking
   "Files", "My Computer", or by pressing the keyboard shortcut `Win+E`.
2. In this window, navigate to your Minecraft data folder. On Windows, you can
   do this by putting `%APPDATA%\.minecraft` into the window's address bar.
   (The `%APPDATA%` part is a *variable* that stands for the path where most
   programs dump their savefiles, configs, profiles, etc. It's most often
   something like `C:\Users\Your Name\AppData\Roaming`, where `AppData` is a
   hidden folder.)
3. You know you're right when you see subfolders such as "bin", "config" and
   "saves".
4. If there is already a folder "mods" in there (maybe because of our previous
   run), delete all files in it. If there *isn't* a folder "mods", create it.
5. Copy the files from the ZIP file's folder "required-mods" over to your
   Minecraft's "mods" folder.
6. If you want any optional mods, copy them from "extra-mods" over into into
   "mods". The [title page](README.md#optional-mods) describes each of them.

Joining
-------

1. Pull up the Minecraft launcher and make sure that you run the Minecraft
   version "fabric-loader-1.18.1".
2. In the server list, you might have a red X next to our server about
   incompatible FML plugins, but you should still be able to join.
