Society of Prickly Plants – Minecraft Modpack
=============================================

This is the modpack used by the SOPP Minecraft server. It is maintained only
for this one individual server. Updates are made whenever I feel like it.

- [Installation instructions](INSTALL.md)
- [Changelog](CHANGELOG.md)

Mod List
--------

Required mods:

- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api)
- [Cloth Config API](https://www.curseforge.com/minecraft/mc-mods/cloth-config),
  as required by You're in Grave Danger
- [You're in Grave Danger](https://www.curseforge.com/minecraft/mc-mods/youre-in-grave-danger)
  ([Wiki](https://github.com/B1n-ry/Youre-in-grave-danger/wiki))
- [Camp Chair](https://www.curseforge.com/minecraft/mc-mods/camp-chair)
- [Fabric Waystones](https://www.curseforge.com/minecraft/mc-mods/fabric-waystones)

Optional mods:

- [Xaero's Minimap](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap)
- [Xaero's World Map](https://www.curseforge.com/minecraft/mc-mods/xaeros-world-map)
- [Iris](https://modrinth.com/mod/iris)
  to add shader support (requires Sodium, see below)
- [Lithium](https://www.curseforge.com/minecraft/mc-mods/lithium)
  to optimize the game physics without changing mechanics
- [Phosphor](https://www.curseforge.com/minecraft/mc-mods/phosphor)
  to optimize the lighting engine without changing mechanics
- [Sodium](https://www.curseforge.com/minecraft/mc-mods/sodium)
  to improve game performance without changing mechanics

Server-side mods:

- [Dynmap](https://www.curseforge.com/minecraft/mc-mods/dynmapforge/)
  ([Wiki](https://github.com/webbukkit/dynmap/wiki))
- [When Dungeons Arise](https://www.curseforge.com/minecraft/mc-mods/when-dungeons-arise-fabric)
- [Towers of the Wild: Reworked](https://www.curseforge.com/minecraft/mc-mods/towers-of-the-wild-reworked)
