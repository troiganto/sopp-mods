Changelog
=========

03 April 2022
-------------

- Updated Xaero's Minimap to 22.3.0 (full edition)
- Added Iris 1.2.0 as *optional* mod

02 April 2022
-------------

- New server: switch to Fabric and new set of mods

19 December 2020
----------------

- Updated Forge to 34.1.42 (was 34.1.0)
- Updated Biomes o' Plenty to 12.0.0.417 (was 12.0.0.409)
- Updated Gravestone to 2.0.4 (was 1.0.2)
- Updated optional OptiFine to G5 (was G3)
- Added Serene Seasons 4.0.0.78
- Added Xaero's Minimap 20.29.1 as *optional* mod
- Added Xaero's World Map 1.11.4 as *optional* mod

17 December 2020
----------------

- Added Construction Wand 1.7

10 December 2020
----------------

- Changed Morpheus sleep quorum to 75 % (was 100 %)

21 November 2020
----------------

- Updated Waystones from to 7.3.1 (was 7.3.0)
- Disabled auto-removal of obituaries
- Fixed Waystones experience cost. The new cost is between 0.1 and 3
  levels. 1000 blocks cost 1 level. Warping between dimensions always costs 3
  levels flat. Warpstone warps are 50 % more expensive. Warp scroll warps are
  50 % cheaper.
- Changed Morpheus sleep quorum to 100 % (was 66 %)
- Changed Morpheus miner threshold to `Y ≤ 60` (was `Y ≤ 64`)

15 November 2020
----------------

- Turned the bastard fox into a chicken

26 October 2020
---------------

- Imprisoned the bastard fox for crimes against poultry

24 October 2020
---------------

**Note:** This update contains a breaking change to terrain generation. new
chunks will be discontinuous with past ones.

- Updated Minecraft to 1.16.3 (was 1.16.1)
- Updated Forge to 34.1.0 (was 32.0.108)

16 August 2020
--------------

Initial release
